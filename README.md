# Doctor case review API

## TLDR

The demo aplication can be found in here:<br>
https://api.webdatapower.tk/swagger/

<b>User:</b> doctor@admin.com<br>
<b>Password:</b> P@ssw0rd

![swagger](images/swagger.jpg)

## Description

In this repository you can find the API that will be the support base for the frontend module.

All requirements have been added to the gitlab issues board which can be viewed [here](https://gitlab.com/groups/ruben-challenge/-/boards).

To protect the api, Firebase Authentication was used so that only a user with a valid JWT can access the api's resources.

![firebase](images/firebase.png)

For the database the application is using a shared database in Mongo Atlas to be easy to deploy.

![mongo](images/mongo.jpg)

This application was also deployed in a private kubernetes custer installed in the Google Cloud Platform and integrated with [Gitlab pipelines](https://gitlab.com/ruben-challenge/backend/-/pipelines). All of this has been done as a bonus so that it can be consulted and used for testing.

![gitlab](images/gitlab.jpg)

![gke](images/gke.jpg)

## Implemented Features
* Support for swagger;
* Migrations to inject data in the database;
* Authentication based on JWT token using Firebase auth;
* Endpoint to authenticate users in firebase;
* Endpoint to get users information from firebase;
* Endpoint to get the application health for kubernetes deployment;
* Endpoint to get all the conditions existing in the database;
* Endpoint to get the next available case;
* Endpoint to update the case information by id;

## Installation

```bash
$ npm install
```

## Configuration

Change inside the `.env` file the connection string for mongodb.

## Running the app

```bash
# migrations
$ npm run migrate:up

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Using docker-compose

```bash
# start containers
$ docker-compose up -d
```