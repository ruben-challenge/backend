import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import * as contextService from "request-context";
import { ExpressAdapter } from "@nestjs/platform-express";
import * as express from "express";
import * as helmet from "helmet";
import { ConfigService } from "@nestjs/config";

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv").config({ debug: true });

const server: express.Express = express();

const corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

const createNestServer = async (expressInstance: any) => {
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(expressInstance)
  );

  const configService = app.get(ConfigService);

  const options = new DocumentBuilder()
    .setTitle("Public api")
    .setDescription("The API description")
    .setVersion("v1")
    .addBearerAuth()
    .build();

  app.use(contextService.middleware("request"));

  app.use(helmet());
  app.use(express.json({ limit: "50mb" }));
  app.enableShutdownHooks();
  app.enableCors(corsOptions);

  const document = SwaggerModule.createDocument(app, options, {
    ignoreGlobalPrefix: false,
    deepScanRoutes: true,
  });

  SwaggerModule.setup("swagger", app, document);

  const port = configService.get("app.port");
  await app.listen(port, () =>
    console.log(`Application is listening on port ${port}.`)
  );
};

createNestServer(server)
  .then(() => console.log("Nest Ready"))
  .catch((err) => console.error("Nest broken", err));
