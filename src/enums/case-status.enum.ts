export enum CaseStatusType {
  available = 1,
  locked = 2,
  reviewed = 3,
}
