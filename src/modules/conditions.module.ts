import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConditionsController } from "../controllers/conditions.controller";
import { ConditionsRepository } from "../repositories/conditions.repository";
import { ConditionsService } from "../services/conditions.service";
import { ConditionsSchema } from "../entities/conditions.entity";

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: "condition",
        schema: ConditionsSchema,
        collection: "conditions",
      },
    ]),
  ],
  controllers: [ConditionsController],
  providers: [ConditionsService, ConditionsRepository],
  exports: [ConditionsService, ConditionsRepository],
})
export class ConditionsModule {}
