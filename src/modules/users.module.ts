import { Module } from "@nestjs/common";
import { UsersRepository } from "../repositories/users.repository";
import { UsersController } from "src/controllers/users.controller";
import { UsersService } from "src/services/users.service";

@Module({
  imports: [],
  controllers: [UsersController],
  providers: [UsersService, UsersRepository],
  exports: [UsersService, UsersRepository],
})
export class UsersModule {}
