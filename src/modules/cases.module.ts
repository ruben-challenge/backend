import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { CasesSchema } from "../entities/cases.entity";
import { CasesController } from "../controllers/case.controller";
import { CasesService } from "../services/cases.service";
import { CasesRepository } from "../repositories/cases.repository";

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: "case", schema: CasesSchema, collection: "cases" },
    ]),
  ],
  controllers: [CasesController],
  providers: [CasesService, CasesRepository],
  exports: [CasesService, CasesRepository],
})
export class CasesModule {}
