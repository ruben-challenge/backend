import { createParamDecorator } from "@nestjs/common";
import { JwtDTO } from "../../dtos/jwt.dto";
import { Request } from "express";

import * as contextService from "request-context";

export const RequestUser = createParamDecorator(
  (_data, req: Request): JwtDTO => {
    // inject user in http context
    const user = contextService.get("user");

    return user;
  }
);
