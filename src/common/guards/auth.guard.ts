import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import * as contextService from "request-context";

@Injectable()
export class AuthenticationGuard implements CanActivate {
  public async canActivate(context: ExecutionContext) {
    const request: any = context.switchToHttp().getRequest();

    if (!request.user) {
      console.log(`No user found for request: ${request.path}`);
    }

    // inject user in http context
    contextService.set("user", request.user);

    return !!request.user;
  }
}
