import { PassportStrategy } from "@nestjs/passport";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { auth, credential, initializeApp } from "firebase-admin";
import { Strategy, ExtractJwt } from "passport-firebase-jwt";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        ExtractJwt.fromUrlQueryParameter("access_token"),
      ]),
    });

    //initialize firebase inorder to access its services
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const serviceAccount = require("../../../firebase-sdk.json");

    initializeApp({
      credential: credential.cert(serviceAccount),
    });
  }

  async validate(payload: string) {
    try {
      const result = await auth().verifyIdToken(payload, true);

      if (!result) {
        throw new UnauthorizedException();
      }

      return result;
    } catch (error) {
      //console.log(error);
      throw new UnauthorizedException();
    }
  }
}
