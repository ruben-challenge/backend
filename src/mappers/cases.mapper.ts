import { GetAvailableCaseDto } from "../dtos/cases/getAvailableCase.dto";
import { Case } from "../entities/cases.entity";

export class CasesMapper {
  public static toModel(dto: GetAvailableCaseDto): Case {
    if (dto == null) {
      return null;
    }

    return new Case(
      dto.id,
      dto.ehr,
      dto.status,
      null,
      null,
      dto.created_at,
      dto.updated_at
    );
  }

  public static toDto(model: Case): GetAvailableCaseDto {
    if (model == null) {
      return null;
    }

    return new GetAvailableCaseDto(
      model.id,
      model.ehr,
      model.status,
      model.created_at,
      model.updated_at
    );
  }
}
