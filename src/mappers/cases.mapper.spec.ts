import { CasesMapper } from "./cases.mapper";
import { GetAvailableCaseDto } from "../dtos/cases/getAvailableCase.dto";
import { Case } from "../entities/cases.entity";

describe("CasesMapper", () => {
  it("should return null when convert dto to model", () => {
    const dto: GetAvailableCaseDto = null;

    const convertedModel = CasesMapper.toModel(dto);

    expect(convertedModel).toBeNull();
  });

  it("should return null when convert model to dto", () => {
    const model: Case = null;

    const convertedDto = CasesMapper.toDto(model);

    expect(convertedDto).toBeNull();
  });
});
