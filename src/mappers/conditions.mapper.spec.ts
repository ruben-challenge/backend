import { ConditionsMapper } from "./conditions.mapper";
import { ConditionsDto } from "../dtos/conditions/conditions.dto";
import { Condition } from "../entities/conditions.entity";

describe("ConditionsMapper", () => {
  it("should return null when convert dto to model", () => {
    const dto: ConditionsDto = null;

    const convertedModel = ConditionsMapper.toModel(dto);

    expect(convertedModel).toBeNull();
  });

  it("should return null when convert model to dto", () => {
    const model: Condition = null;

    const convertedDto = ConditionsMapper.toDto(model);

    expect(convertedDto).toBeNull();
  });
});
