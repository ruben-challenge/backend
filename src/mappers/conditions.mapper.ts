import { ConditionsDto } from "../dtos/conditions/conditions.dto";
import { Condition } from "../entities/conditions.entity";

export class ConditionsMapper {
  public static toModel(dto: ConditionsDto): Condition {
    if (dto == null) {
      return null;
    }

    return new Condition(
      dto.id.toString(),
      dto.code,
      dto.title,
      dto.created_at
    );
  }

  public static toDto(model: Condition): ConditionsDto {
    if (model == null) {
      return null;
    }

    return new ConditionsDto(
      model.id,
      model.code,
      model.title,
      model.created_at
    );
  }
}
