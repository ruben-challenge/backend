import {
  Get,
  HttpStatus,
  UseGuards,
  HttpException,
  Controller,
  HttpCode,
  Post,
  Body,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { JwtDTO } from "../dtos/jwt.dto";
import { RequestUser } from "../common/decorators/request-user.decorator";
import { AuthenticationGuard } from "../common/guards/auth.guard";
import { AuthLoginDto } from "../dtos/users/login.dto";
import { User } from "@firebase/auth-types";
import { AuthService } from "src/services/auth.service";

@Controller("auth")
@ApiTags("auth")
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post("login")
  @ApiOperation({
    summary: "Login a user",
    description: "Login a user",
  })
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK })
  async login(@Body() user: AuthLoginDto): Promise<User> {
    return this.authService.login(user.email, user.password);
  }

  @Get("me")
  @ApiBearerAuth()
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: "Unauthorized.",
    type: HttpException,
  })
  @ApiOperation({
    summary: "Retrieve the user information",
    description: "Retrieve the user information",
  })
  @ApiResponse({ status: HttpStatus.FORBIDDEN, description: "Forbidden." })
  @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request." })
  @UseGuards(AuthGuard("firebase-jwt"), AuthenticationGuard)
  async me(@RequestUser() user: JwtDTO): Promise<JwtDTO> {
    return user;
  }
}
