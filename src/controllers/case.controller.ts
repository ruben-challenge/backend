import {
  Get,
  HttpStatus,
  UseGuards,
  HttpException,
  Controller,
  HttpCode,
  Param,
  Patch,
  NotFoundException,
  Body,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { JwtDTO } from "../dtos/jwt.dto";
import { RequestUser } from "../common/decorators/request-user.decorator";
import { AuthenticationGuard } from "../common/guards/auth.guard";
import { CasesService } from "../services/cases.service";
import { GetAvailableCaseDto } from "../dtos/cases/getAvailableCase.dto";
import { UpdateCaseDto } from "../dtos/cases/updateCase.dto";

@Controller("case")
@ApiTags("case")
@UseGuards(AuthGuard("firebase-jwt"), AuthenticationGuard)
@ApiResponse({
  status: HttpStatus.UNAUTHORIZED,
  description: "Unauthorized.",
  type: HttpException,
})
@ApiResponse({ status: HttpStatus.FORBIDDEN, description: "Forbidden." })
@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request." })
@ApiBearerAuth()
export class CasesController {
  constructor(private readonly casesService: CasesService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: GetAvailableCaseDto })
  @ApiOperation({
    summary: "Get next free case",
    description: "Get next free case",
  })
  async getOne(@RequestUser() user: JwtDTO): Promise<GetAvailableCaseDto> {
    const result = await this.casesService.findNextAvailable(user.uid);

    if (result == null) {
      throw new NotFoundException();
    }

    return result;
  }

  @Patch(":id")
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: GetAvailableCaseDto })
  @ApiOperation({
    summary: "Update case status and labels",
    description: "Update case status and labels",
  })
  async update(
    @RequestUser() user: JwtDTO,
    @Param("id") id: string,
    @Body() data: UpdateCaseDto
  ): Promise<GetAvailableCaseDto> {
    const result = await this.casesService.reviewed(user.uid, id, data);

    if (result == null) {
      throw new NotFoundException();
    }

    return result;
  }
}
