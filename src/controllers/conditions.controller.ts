import {
  Get,
  HttpStatus,
  UseGuards,
  HttpException,
  Controller,
  HttpCode,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiResponse, ApiTags } from "@nestjs/swagger";
import { AuthenticationGuard } from "src/common/guards/auth.guard";
import { ConditionsDto } from "src/dtos/conditions/conditions.dto";
import { ConditionsService } from "src/services/conditions.service";

@Controller("conditions")
@ApiTags("conditions")
@UseGuards(AuthGuard("firebase-jwt"), AuthenticationGuard)
@ApiResponse({
  status: HttpStatus.UNAUTHORIZED,
  description: "Unauthorized.",
  type: HttpException,
})
@ApiResponse({ status: HttpStatus.FORBIDDEN, description: "Forbidden." })
@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request." })
@ApiBearerAuth()
export class ConditionsController {
  constructor(private readonly conditionsService: ConditionsService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: ConditionsDto, isArray: true })
  async getAll(): Promise<ConditionsDto[]> {
    try {
      return await this.conditionsService.findAll();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }
}
