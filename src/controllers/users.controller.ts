import {
  Get,
  HttpStatus,
  UseGuards,
  HttpException,
  Controller,
  HttpCode,
  Put,
  Body,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { JwtDTO } from "../dtos/jwt.dto";
import { RequestUser } from "../common/decorators/request-user.decorator";
import { AuthenticationGuard } from "../common/guards/auth.guard";
import { auth } from "firebase-admin";
import { UserDto } from "src/dtos/users/user.dto";
import { UpdateUserDto } from "src/dtos/users/updateUser.dto";
import { UsersService } from "src/services/users.service";

@Controller("users")
@ApiTags("users")
@UseGuards(AuthGuard("firebase-jwt"), AuthenticationGuard)
@ApiResponse({
  status: HttpStatus.UNAUTHORIZED,
  description: "Unauthorized.",
  type: HttpException,
})
@ApiResponse({ status: HttpStatus.FORBIDDEN, description: "Forbidden." })
@ApiResponse({ status: HttpStatus.BAD_REQUEST, description: "Bad Request." })
@ApiBearerAuth()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiResponse({ status: HttpStatus.OK, type: UserDto })
  @ApiOperation({
    summary: "Get informations about the current user",
    description: "Get informations about the current user",
  })
  async get(@RequestUser() user: JwtDTO): Promise<UserDto> {
    return await this.usersService.findById(user.uid);
  }

  @Put()
  @HttpCode(HttpStatus.ACCEPTED)
  @ApiOperation({
    summary: "Update information from the current user",
    description: "Update information the current user",
  })
  async update(
    @RequestUser() user: JwtDTO,
    @Body() data: UpdateUserDto
  ): Promise<auth.UserRecord> {
    return await this.usersService.update(user.uid, data);
  }
}
