import { ApiProperty } from "@nestjs/swagger";

export class ConditionsDto {
  constructor(id: string, code: string, title: string, created_at: Date) {
    this.id = id;
    this.code = code;
    this.title = title;
    this.created_at = created_at;
  }

  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly code: string;

  @ApiProperty()
  readonly title: string;

  @ApiProperty()
  readonly created_at: Date;
}
