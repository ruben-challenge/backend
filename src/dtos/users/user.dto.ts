import { UserInfo, UserMetadata } from "@firebase/auth-types";
import { ApiProperty } from "@nestjs/swagger";

export class UserDto {
  /**
   * The user's `uid`.
   */
  @ApiProperty()
  uid: string;
  /**
   * The user's primary email, if set.
   */
  @ApiProperty()
  email?: string;
  /**
   * Whether or not the user's primary email is verified.
   */
  @ApiProperty()
  emailVerified: boolean;
  /**
   * The user's display name.
   */
  @ApiProperty()
  displayName?: string;
  /**
   * The user's primary phone number, if set.
   */
  @ApiProperty()
  phoneNumber?: string;
  /**
   * The user's photo URL.
   */
  @ApiProperty()
  photoURL?: string;
  /**
   * Whether or not the user is disabled: `true` for disabled; `false` for
   * enabled.
   */
  @ApiProperty()
  disabled: boolean;
  /**
   * Additional metadata about the user.
   */
  @ApiProperty()
  metadata: UserMetadata;
  /**
   * An array of providers (for example, Google, Facebook) linked to the user.
   */
  @ApiProperty()
  providerData: UserInfo[];
  /**
   * The user's hashed password (base64-encoded), only if Firebase Auth hashing
   * algorithm (SCRYPT) is used. If a different hashing algorithm had been used
   * when uploading this user, as is typical when migrating from another Auth
   * system, this will be an empty string. If no password is set, this is
   * null. This is only available when the user is obtained from
   * {@link auth.Auth.listUsers `listUsers()`}.
   *
   */
  @ApiProperty()
  passwordHash?: string;
  /**
   * The user's password salt (base64-encoded), only if Firebase Auth hashing
   * algorithm (SCRYPT) is used. If a different hashing algorithm had been used to
   * upload this user, typical when migrating from another Auth system, this will
   * be an empty string. If no password is set, this is null. This is only
   * available when the user is obtained from
   * {@link auth.Auth.listUsers `listUsers()`}.
   *
   */
  @ApiProperty()
  passwordSalt?: string;
  /**
   * The user's custom claims object if available, typically used to define
   * user roles and propagated to an authenticated user's ID token.
   * This is set via
   * {@link auth.Auth.setCustomUserClaims `setCustomUserClaims()`}
   */
  @ApiProperty()
  customClaims?: {
    [key: string]: any;
  };
  /**
   * The date the user's tokens are valid after, formatted as a UTC string.
   * This is updated every time the user's refresh token are revoked either
   * from the {@link auth.Auth.revokeRefreshTokens `revokeRefreshTokens()`}
   * API or from the Firebase Auth backend on big account changes (password
   * resets, password or email updates, etc).
   */
  @ApiProperty()
  tokensValidAfterTime?: string;
  /**
   * The ID of the tenant the user belongs to, if available.
   */
  @ApiProperty()
  tenantId?: string | null;
}
