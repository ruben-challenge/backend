import { ApiProperty } from "@nestjs/swagger";

export class UpdateUserDto {
  @ApiProperty()
  displayName: string;

  @ApiProperty()
  emailVerified: boolean;
}
