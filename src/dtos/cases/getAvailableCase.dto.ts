import { ApiProperty } from "@nestjs/swagger";
import { CaseStatusType } from "../../enums/case-status.enum";

export class GetAvailableCaseDto {
  constructor(
    id: string,
    ehr: string,
    status: CaseStatusType,
    created_at: Date,
    updated_at: Date
  ) {
    this.id = id;
    this.ehr = ehr;
    this.status = status;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }

  @ApiProperty()
  readonly id: string;

  @ApiProperty()
  readonly ehr: string;

  @ApiProperty()
  readonly status: CaseStatusType;

  @ApiProperty()
  readonly created_at: Date;

  @ApiProperty()
  readonly updated_at: Date;
}
