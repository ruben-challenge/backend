import { ApiProperty } from "@nestjs/swagger";

export class UpdateCaseDto {
  @ApiProperty()
  readonly labels: string[];
}
