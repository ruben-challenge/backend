export class JwtDTO {
  readonly uid!: string;

  readonly role!: string;

  readonly email!: string;
}
