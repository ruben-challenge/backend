import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Condition } from "../entities/conditions.entity";

@Injectable()
export class ConditionsRepository {
  constructor(
    @InjectModel("condition")
    private readonly conditionsModel: Model<Condition>
  ) {}

  async findAll(): Promise<Condition[]> {
    return this.conditionsModel.find().exec();
  }
}
