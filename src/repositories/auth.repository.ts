import { Injectable } from "@nestjs/common";
import firebase from "firebase";
import { User } from "@firebase/auth-types";

@Injectable()
export class AuthRepository {
  constructor() {
    //initialize firebase inorder to access its services
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const firebaseConfig = require("../../firebase-config.json");

    firebase.initializeApp(firebaseConfig);
  }

  async login(email: string, password: string): Promise<User> {
    const result = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password);

    return result.user;
  }
}
