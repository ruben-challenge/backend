import { Test, TestingModule } from "@nestjs/testing";
import { CasesRepository } from "./cases.repository";
import {
  closeInMongodConnection,
  rootMongooseTestModule,
} from "../test-utils/mongo/MongooseTestModule";
import { MongooseModule } from "@nestjs/mongoose";
import { CasesSchema } from "../entities/cases.entity";

describe("CaseRepository", () => {
  let provider: CasesRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        rootMongooseTestModule(),
        MongooseModule.forFeature([
          { name: "case", schema: CasesSchema, collection: "cases" },
        ]),
      ],
      providers: [CasesRepository],
    }).compile();

    provider = module.get<CasesRepository>(CasesRepository);
  });

  afterAll(async () => {
    await closeInMongodConnection();
  });

  it("should be defined", () => {
    expect(provider).toBeDefined();
  });
});
