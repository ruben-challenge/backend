import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CaseStatusType } from "../enums/case-status.enum";
import { Case } from "../entities/cases.entity";

@Injectable()
export class CasesRepository {
  constructor(
    @InjectModel("case")
    private readonly casesModel: Model<Case>
  ) {}

  async findNextAvailable(userId: string): Promise<Case> {
    let result = await this.casesModel
      .findOne({ status: CaseStatusType.locked, doctor: userId })
      .exec();

    if (result == null) {
      result = await this.casesModel
        .findOne({ status: CaseStatusType.available })
        .exec();
    }

    if (result != null) {
      await this.update(userId, result.id, CaseStatusType.locked);
    }

    return result;
  }

  async findById(caseId: string): Promise<Case> {
    return this.casesModel.findOne({ _id: caseId }).exec();
  }

  async update(
    userId: string,
    caseId: string,
    status: CaseStatusType,
    labels?: string[]
  ): Promise<void> {
    await this.casesModel
      .updateOne(
        { _id: caseId },
        {
          status: status,
          labels: labels,
          doctor: userId,
          updated_at: new Date(),
        }
      )
      .exec();
  }
}
