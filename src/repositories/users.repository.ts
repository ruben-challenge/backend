import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CaseStatusType } from "../enums/case-status.enum";
import { Case } from "../entities/cases.entity";
import { auth } from "firebase-admin";
import { UpdateUserDto } from "src/dtos/users/updateUser.dto";

@Injectable()
export class UsersRepository {
  async findById(userId: string): Promise<auth.UserRecord> {
    return auth().getUser(userId);
  }

  async update(userId: string, data: UpdateUserDto): Promise<auth.UserRecord> {
    return auth().updateUser(userId, data);
  }
}
