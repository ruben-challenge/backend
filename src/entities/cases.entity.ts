import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { IsMongoId, IsOptional, IsString } from "class-validator";
import { Document, Types } from "mongoose";
import { CaseStatusType } from "../enums/case-status.enum";

@Schema()
export class Case extends Document {
  constructor(
    id: string,
    ehr: string,
    status: CaseStatusType,
    doctor: string,
    labels: string[],
    created_at: Date,
    updated_at: Date
  ) {
    super();
    this._id = id;
    this.ehr = ehr;
    this.status = status;
    this.doctor = doctor;
    this.labels = labels;
    this.created_at = created_at;
    this.updated_at = updated_at;
  }

  @Prop({ required: true })
  ehr: string;

  @Prop({ required: true })
  status: CaseStatusType;

  @Prop({ default: "" })
  doctor?: string;

  @Prop({ default: [] })
  labels: string[];

  @Prop({ required: true })
  created_at: Date;

  @Prop({ required: true })
  updated_at: Date;

  public setCaselock() {
    this.status = CaseStatusType.locked;
    this.updated_at = new Date();
  }
}

export const CasesSchema = SchemaFactory.createForClass(Case);
