import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

@Schema()
export class Condition extends Document {
  constructor(id: string, code: string, title: string, created_at: Date) {
    super();
    this._id = id;
    this.code = code;
    this.title = title;
    this.created_at = created_at;
  }

  @Prop({ required: true })
  code: string;

  @Prop({ required: true })
  title: string;

  @Prop({ default: Date.now })
  created_at: Date;
}

export const ConditionsSchema = SchemaFactory.createForClass(Condition);
