import { mongo } from "mongoose";
import { getDb } from "../migrations-utils/db-connector.helper";

const ObjectID = mongo.ObjectID;

export const up = async () => {
  const db = await getDb();
  /*
      Code your update script here!
   */
};

export const down = async () => {
  const db = await getDb();
  /*
      Code you downgrade script here!
   */
};
