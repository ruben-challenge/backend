import { MongoClient } from "mongodb";
import configs from "../../config/database.config";

const MONGO_URL = configs().uri;

export const getDb = async () => {
  const client: MongoClient = await MongoClient.connect(MONGO_URL, {
    useUnifiedTopology: true,
  });
  return client.db();
};
