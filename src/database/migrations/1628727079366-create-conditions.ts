import { mongo } from "mongoose";
import { getDb } from "../migrations-utils/db-connector.helper";
import * as fs from "fs";
import * as path from "path";
import * as csv from "csv-parser";

const ObjectID = mongo.ObjectID;

export const up = async () => {
  const db = await getDb();
  const conditions = db.collection("conditions");

  const results = await getCSVData();

  const result = await conditions.insertMany(results);

  console.log(`${result.insertedCount} documents were inserted`);
};

export const down = async () => {
  const db = await getDb();
  /*
      Code you downgrade script here!
   */
};

function getCSVData(): Promise<Array<any>> {
  return new Promise((resolve, reject) => {
    const list = [];
    fs.createReadStream(path.join(__dirname, "..", "data", "conditions.csv"))
      .pipe(csv({ separator: ";" }))
      .on("data", (data) => {
        list.push({ ...data, _id: new ObjectID(), created_at: new Date() });
      })
      .on("error", reject)
      .on("end", () => resolve(list));
  });
}
