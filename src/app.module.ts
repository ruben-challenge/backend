import {
  HelmetHidePoweredByMiddleware,
  HelmetMiddleware,
} from "@nest-middlewares/helmet";
import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { PassportModule } from "@nestjs/passport";
import { TerminusModule } from "@nestjs/terminus";
import { ResponseTimeMiddleware } from "@nest-middlewares/response-time";

import {
  PassportInitializeMiddleware,
  PassportSessionMiddleware,
} from "@nest-middlewares/passport";
import { CorsMiddleware } from "@nest-middlewares/cors";
import { MongooseModule, MongooseModuleOptions } from "@nestjs/mongoose";
import { JwtStrategy } from "./common/strategy/firebase-auth.strategy";
import appConfig from "./config/app.config";
import databaseConfig from "./config/database.config";
import { ConditionsModule } from "./modules/conditions.module";
import { CasesModule } from "./modules/cases.module";
import { AuthModule } from "./modules/auth.module";
import { UsersModule } from "./modules/users.module";
import { HealthController } from "./controllers/health.controller";
import { LoggerModule } from "nestjs-pino";
@Module({
  imports: [
    LoggerModule.forRoot(),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [appConfig, databaseConfig],
    }),
    PassportModule.register({
      defaultStrategy: "firebase-jwt",
    }),
    TerminusModule,
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get<MongooseModuleOptions>("database"),
    }),
    ConditionsModule,
    CasesModule,
    AuthModule,
    UsersModule,
    TerminusModule,
  ],
  controllers: [HealthController],
  providers: [JwtStrategy],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    HelmetMiddleware.configure({ hidePoweredBy: true });
    consumer
      .apply(HelmetMiddleware, HelmetHidePoweredByMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });

    CorsMiddleware.configure({
      origin: false,
      allowedHeaders: ["Access-Control-Allow-*"],
      exposedHeaders: ["X-Total-Count"],
    });
    consumer
      .apply(CorsMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });

    consumer
      .apply(ResponseTimeMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });

    consumer
      .apply(PassportInitializeMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL })
      .apply(PassportSessionMiddleware)
      .forRoutes({ path: "*", method: RequestMethod.ALL });
  }
}
