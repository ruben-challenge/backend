import { Injectable, Scope } from "@nestjs/common";
import { ConditionsDto } from "../dtos/conditions/conditions.dto";
import { ConditionsMapper } from "../mappers/conditions.mapper";
import { ConditionsRepository } from "../repositories/conditions.repository";

@Injectable({ scope: Scope.REQUEST })
export class ConditionsService {
  constructor(private readonly conditionsRepository: ConditionsRepository) {}

  async findAll(): Promise<ConditionsDto[]> {
    const result = await this.conditionsRepository.findAll();

    return result.map((condition) => ConditionsMapper.toDto(condition));
  }
}
