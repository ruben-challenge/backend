import { User } from "@firebase/auth-types";
import { Injectable, NotFoundException, Scope } from "@nestjs/common";
import { AuthRepository } from "src/repositories/auth.repository";

@Injectable({ scope: Scope.REQUEST })
export class AuthService {
  constructor(private readonly authRepository: AuthRepository) {}

  async login(email: string, password: string): Promise<User> {
    return this.authRepository.login(email, password);
  }
}
