import { Injectable, NotFoundException, Scope } from "@nestjs/common";
import { GetAvailableCaseDto } from "src/dtos/cases/getAvailableCase.dto";
import { UpdateCaseDto } from "src/dtos/cases/updateCase.dto";
import { CaseStatusType } from "src/enums/case-status.enum";
import { CasesMapper } from "src/mappers/cases.mapper";
import { CasesRepository } from "src/repositories/cases.repository";

@Injectable({ scope: Scope.REQUEST })
export class CasesService {
  constructor(private readonly casesRepository: CasesRepository) {}

  async findNextAvailable(userId: string): Promise<GetAvailableCaseDto> {
    const result = await this.casesRepository.findNextAvailable(userId);

    return CasesMapper.toDto(result);
  }

  async reviewed(
    userId: string,
    caseId: string,
    updateCase: UpdateCaseDto
  ): Promise<GetAvailableCaseDto> {
    const result = await this.casesRepository.findById(caseId);

    if (result == null) {
      throw new NotFoundException();
    }

    await this.casesRepository.update(
      userId,
      caseId,
      CaseStatusType.reviewed,
      updateCase.labels
    );

    return CasesMapper.toDto(result);
  }
}
