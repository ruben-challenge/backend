import { Injectable, Scope } from "@nestjs/common";
import { auth } from "firebase-admin";
import { UpdateUserDto } from "src/dtos/users/updateUser.dto";
import { UserDto } from "src/dtos/users/user.dto";
import { UsersRepository } from "src/repositories/users.repository";

@Injectable({ scope: Scope.REQUEST })
export class UsersService {
  constructor(private readonly usersRepository: UsersRepository) {}

  async findById(userId: string): Promise<UserDto> {
    const result = await this.usersRepository.findById(userId);

    return result;
  }

  async update(userId: string, data: UpdateUserDto): Promise<auth.UserRecord> {
    const result = await this.usersRepository.update(userId, data);

    return result;
  }
}
