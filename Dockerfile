FROM node:12.13-alpine AS builder

RUN apk add --update --no-cache curl tzdata python2 python3 make libmagic build-base

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci

COPY tsconfig*.json ./

COPY src src

RUN npm run build



FROM node:12.13-alpine  as production

ENV NODE_ENV=production
RUN apk add --no-cache tini

WORKDIR /usr/src/app

RUN chown node:node .

USER node

COPY package*.json ./

RUN npm install

COPY --from=builder /usr/src/app/dist/ dist/
COPY firebase*.json ./
COPY .env ./

EXPOSE 5000

ENTRYPOINT [ "/sbin/tini","--", "node", "dist/main.js" ]